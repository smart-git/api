﻿/*
Navicat MySQL Data Transfer

Source Server         : new
Source Server Version : 80012
Source Host           : localhost:3306
Source Database       : api

Target Server Type    : MYSQL
Target Server Version : 80012
File Encoding         : 65001

Date: 2018-09-10 15:18:22
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for api_interface
-- ----------------------------
DROP TABLE IF EXISTS api_interface;
CREATE TABLE api_interface (
  api_id int(11) NOT NULL AUTO_INCREMENT COMMENT 'id主键自动增长',
  api_method varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '接口方法',
  api_params varchar(255) DEFAULT NULL,
  api_result_content varchar(255) DEFAULT NULL,
  api_description varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '接口描述',
  api_use_flag int(2) DEFAULT NULL COMMENT '接口注册标志',
  api_del_flag int(2) DEFAULT NULL COMMENT '接口是否删除',
  api_time datetime DEFAULT NULL,
  api_type int(6) DEFAULT NULL,
  api_type_name varchar(255) DEFAULT NULL,
  api_req_mode varchar(255) DEFAULT NULL COMMENT '接口请求方式',
  api_order int(11) DEFAULT NULL,
  remark varchar(255) DEFAULT NULL,
  PRIMARY KEY (api_id,api_method),
  UNIQUE KEY api_method (api_method)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
