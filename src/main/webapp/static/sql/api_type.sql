﻿/*
Navicat MySQL Data Transfer

Source Server         : new
Source Server Version : 80012
Source Host           : localhost:3306
Source Database       : api

Target Server Type    : MYSQL
Target Server Version : 80012
File Encoding         : 65001

Date: 2018-09-10 15:18:30
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for api_type
-- ----------------------------
DROP TABLE IF EXISTS api_type;
CREATE TABLE api_type (
  ID int(11) NOT NULL AUTO_INCREMENT,
  TYPE_NAME varchar(50) DEFAULT NULL COMMENT '类名',
  TYPE_DESC varchar(255) DEFAULT NULL COMMENT '类的描述',
  ORDER int(6) DEFAULT NULL,
  DATE datetime DEFAULT NULL,
  PRIMARY KEY (ID)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
