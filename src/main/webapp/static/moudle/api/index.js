//初始化
$(function(){
	apiIndex.init();
	interFaceList();
});
//   visible: false
var apiColumns = [{
	title: '接口方法',
    field: 'apiMethod',
    sortable: false,
    width: '10%',
    titleTooltip: '接口方法',
    formatter: function (value, row, index) {
    	if(value == null) {
    		return "—";
    }
    	return "<span title='"+value+"'>"+value+"</span>";
    }
},{
	title: '接口参数',
    field: 'apiParams',
    sortable: false,
    width: '10%',
    titleTooltip: '接口参数',
    formatter: function (value, row, index) {
    	if(value == null) {
    		return "—";
    }
    	return "<span title='"+value+"'>"+value+"</span>";
    }
},{
	title: '接口返回内容',
    field: 'apiResultContent',
    sortable: false,
    width: '10%',
    titleTooltip: '接口返回内容',
    formatter: function (value, row, index) {
    	if(value == null) {
    		return "—";
    }
    	return "<span title='"+value+"'>"+value+"</span>";
    }
},{
	title: '接口描述',
    field: 'apiDescription',
    sortable: false,
    width: '10%',
    titleTooltip: '接口描述',
    formatter: function (value, row, index) {
    	if(value == null) {
    		return "—";
    	}
        	return "<span title='"+value+"'>"+value+"</span>";
    }
},{
	title: '接口类型',
    field: 'apiTypeName',
    sortable: false,
    width: '10%',
    titleTooltip: '接口类型',
    formatter: function (value, row, index) {
    	if(value == null) {
    		return "—";
    	}
    	return "<span title='"+value+"'>"+value+"</span>";
    }
}];
var apiIndex = function() {
	//初始表格
	var initApiTable = function () {
		var fTable = $("#apiTable").bootstrapTable({
			method: 'get',
			url: "/api/findListPage",
			dataType: 'json',
	        striped: true, //是否显示行间隔色
	        showRefresh: false,//刷新按钮
	        pagination: true,//是否分页
	        singleSelect: false,
	        dataField: 'rows',
	        pageNumber: 1,
	        showColumns: false,
	        pageSize: 10,//单页记录数
	        pageList: [ 5, 10, 20,"All"],
	        search: false, // 搜索框
	        //resizable: true,
	        sidePagination: 'server', //服务端请求
	        pageNumber: 1, //初始化加载第一页，默认第一页
	        queryParamsType: 'undefined',//查询参数组织方
	        queryParams: queryParams,
	        minimunCountColumns: 2,
	        columns: apiColumns,
	        locale: 'zh-CN'//中文支持
		});
		$("#apiTable").colResizable({
			liveDrag: true,
			minWidth: 100,
			postbackSafe: true
		});
		function queryParams(params){
			return{
				pageSize: params.pageSize,
				pageIndex: params.pageNumber,
				apiMethod:$("#apiMethod").val(),
				apiTypeName:$('#apiTypeName').val(),
				apiDescription:$('#apiDescription').val()
			}
		}
		//事件处理
		window.operateEvents = {
				//失败次数
				'click .failcountnext': function (e, value, row, index) {
					var major=row.major;
					var startTime=row.startTime;
					var endTime=row.endTime;
					failNextIndex.init(major,startTime,endTime);
					$("#failModal").modal({
						show : true,
						backdrop : 'static'
					});
				}
		}
	}
	var bindEvent = function () { 
		$('.timepicker').datetimepicker({
			language: 'zh-CN',
            format: 'yyyy-mm-dd hh',
            autoclose: true,
            todayHighlight: true,
            minuteStep: 60
		});
		$('.add-api-submit').click(function(){
			$("#addApiModal").modal({
				show : true,
				backdrop : 'static'
			});
		});
		$('.add-type-submit').click(function(){
			$("#addTypeModal").modal({
				show : true,
				backdrop : 'static'
			});
		});
		$('.search-submit').click(function(){
			$('#apiTable').bootstrapTable('refresh',{
				url:"/api/findListPage"
			});
			//$('#faultTable').bootstrapTable('selectPage',1);
		});
		$('.add-param-submit').click(function(){
			 var paramType=$('#paramType').val();
			 var paramName=$('#paramName').val();
			 if(paramType==''||paramName==''){
				 swal({
					    title: "参数不能为空!",
					    type: "error",
					    confirmButtonText: "确定"
					});
				 return;
			 }
			 var ps=paramType+","+paramName;
			 var apiParams=$('#apiParams').val();
			 if(apiParams.indexOf(ps)!=-1){
				 swal({
					    title: "参数名称不能相同!",
					    type: "error",
					    confirmButtonText: "确定"
					});
				 return;
			 }
			 if(apiParams!=''&&apiParams!=null){
				 $('#apiParams').val(apiParams+";"+ps);
			 }else{
				 $('#apiParams').val(ps);
			 }
		});
		$('.rem-param-submit').click(function(){
			var apiParams=$('#apiParams').val();
			if(apiParams!=''&&apiParams!=null){
				var i = apiParams.lastIndexOf(';');
				apiParams=apiParams.substring(0,i);
				$('#apiParams').val(apiParams);
			}
		});
		$('.export-submit').click(function(){
			window.location.href = '/api/exportApi?apiMethod='+$("#apiMethod").val()+
									"&apiTypeName="+$('#apiTypeName').val()+
									"&apiDescription="+$('#apiDescription').val();
		});
	}
	return {
		init: function () {
			bindEvent();
			initApiTable();
		}
	}
}();
function interFaceList(){
	$.ajax({
		type:'get',
		url:'/api/getAllType',
		dateType:'json',
		success:function(result){
			if(result!=null){
				$('#apiTypeNameSelect').html('');
				var html="";
				for (var i = 0; i < result.length; i++) {
					html=html+'<option value="'+result[i].typeName+'">'+result[i].typeName+'---'+result[i].typeDesc+'</option>';
				}
				$('#apiTypeNameSelect').html(html);
			}
		}
	});
}
//新增类型
function submitAddType(){
	var typeName=$('#typeNameNew').val();
	if(typeName==''||typeName==null){
		swal({
		    title: "类型名称不能为空!",
		    type: "error",
		    confirmButtonText: "确定"
		});
		return;
	}
	var typeDesc=$('#typeDesc').val();
	if(typeDesc==''||typeDesc==null){
		swal({
		    title: "类型描述不能为空!",
		    type: "error",
		    confirmButtonText: "确定"
		});
		return;
	}
	$.ajax({
		type:'get',
		url:'/api/insertApiType',
		dataType:'json',
		data:{
			typeName:typeName,
			typeDesc:typeDesc
		},
		success:function (data){
			$("#addTypeModal").modal("hide");
			if(data!=null){
				if(data.result){
					swal({
					    title: "新增成功!",
					    type: "info",
					    confirmButtonText: "确定"
					});
					interFaceList();
				}else{
					swal({
					    title: "新增失败!",
					    type: "error",
					    confirmButtonText: "确定"
					});
				}
			}else{
				swal({
				    title: "新增失败!",
				    type: "error",
				    confirmButtonText: "确定"
				});
			}
		}
	});
}
//新增接口
function submitAddApi(){
	var apiMethod=$('#apiMethodAdd').val();
	if(apiMethod==''||apiMethod==null){
		swal({
		    title: "接口方法不能为空!",
		    type: "error",
		    confirmButtonText: "确定"
		});
		return;
	}
	var apiParams=$('#apiParams').val();
	var apiResultContent=$('#apiResultContent').val();
	if(apiResultContent==''||apiResultContent==null){
		swal({
		    title: "接口返回不能为空!",
		    type: "error",
		    confirmButtonText: "确定"
		});
		return;
	}
	var apiDescription=$('#apiDescriptionAdd').val();
	var apiTypeName=$('#apiTypeNameSelect').val();
	var apiReqMode=$('#apiReqMode').val();
	$.ajax({
		type:'get',
		url:'/api/apiRegister',
		dataType:'json',
		data:{
			apiMethod:apiMethod,
			apiParams:apiParams,
			apiResultContent:apiResultContent,
			apiDescription:apiDescription,
			apiTypeName:apiTypeName,
			apiReqMode:apiReqMode
		},
		success:function (data){
			$("#addApiModal").modal("hide");
			if(data!=null){
				if(data.result){
					swal({
						title: "新增成功!",
						type: "info",
						confirmButtonText: "确定"
					});
				}else{
					swal({
						title: "新增失败!",
						type: "error",
						confirmButtonText: "确定"
					});
				}
			}else{
				swal({
					title: "新增失败!",
					type: "error",
					confirmButtonText: "确定"
				});
			}
		}
	});
}