<%@ page contentType="text/html;charset=UTF-8" %>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>主页面</title>
	<meta>
	<link rel="stylesheet" href="/static/bootstrap-sweetalert/sweetalert.css" type="text/css" />
	<link rel="stylesheet" href="/static/bootstrap/3.3.4/css_default/bootstrap.css" type="text/css" />
	<link rel="stylesheet" href="/static/bootstrap-table/bootstrap-table.min.css" type="text/css">
	<link rel="stylesheet" href="/static/bootstrap-select/css/bootstrap-select.min.css" type="text/css" />
	<link rel="stylesheet" href="/static/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" type="text/css" />
	<link rel="stylesheet" href="/static/moudle/api/index.css" type="text/css" />
</head>
<body>
	<div class="tran_management">
		<div class="container col-xs-12">
			<table class="table" id="selectBox">
				<tbody>
						<tr>
							<td>
								<div class="unit-form">
									<span>API方法:</span> 
									<input name="apiMethod" id="apiMethod"/>
								</div>
							</td>
							<td style="position:relative">
								<div class="unit-form">
									<span>类型:</span>
									<input name="apiTypeName" id="apiTypeName"/>
								</div>
							</td>
							<td>
								<div class="unit-form">
									<span>描述:</span> 
									<input name="apiDescription" id="apiDescription"/>
								</div>
							</td>
							<td>
								<div class="unit-form">
								</div>
							</td>
						</tr>
						<tr>
						<td>
							<div class="unit-form" style="position:absolute;top:50px;right:40px;" >
									<button class="add-api-submit" ></button>
									<button class="add-type-submit" ></button>
									<button class="search-submit" ></button>
									<button class="export-submit" ></button>
								</div>
							</td>
						</tr>
				</tbody>
			</table>
		</div>
	    <div class="tran_message col-xs-12">
	    	<div class="tittle col-lg-12"> 
				<span>Api接口列表 </span>
	        </div>
	        <div class="part"></div>
	        <div class="message_push">
	        	<!-- style="table-layout: fixed;" -->
				<table id="apiTable" class="table table-striped table-responsive table-bordered table-hover"></table>
	        </div>
       	</div>
       	<!-- 弹窗 -->
		<div class="modal fade" id="addApiModal" tabindex="-1" role="dialog"
			aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"
							aria-hidden="true">&times;</button>
						<h4 class="modal-title" id="myModalLabel">新增接口</h4>
					</div>
					<div class="modal-body">
						<table id="addApiTable">
							<tr>
								<td class="first">接口方法:</td>
								<td class="second">
									<input id="apiMethodAdd" name="apiMethod">
								</td>
							</tr>
							<tr>
								<td class="first">接口参数:</td>
								<td class="second">
									<input id="apiParams" name="apiParams" disabled="disabled" placeholder="eg:String,name;int,age;String,class">
								</td>
							</tr>
							<tr>
								<td class="first"></td>
								<td class="second">
									<select id="paramType" name="paramType" style="width: 80px;display: block;float: left;">
										<option value="String">String</option>
										<option value="int">int</option>
										<option value="boolean">boolean</option>
										<option value="float">float</option>
										<option value="double">double</option>
									</select>
									<input id="paramName" name="paramName" style="width: 150px;display: block;float: left;">
									<button class="add-param-submit"></button>
									<button class="rem-param-submit"></button>
								</td>
							</tr>
							<tr>
								<td class="first">接口返回:</td>
								<td class="second">
									<textarea id="apiResultContent" name="apiResultContent"></textarea>
								</td>
							</tr>
							<tr>
								<td class="first">接口描述:</td>
								<td class="second">
									<input id="apiDescriptionAdd" name="apiDescription">
								</td>
							</tr>
							<tr>
								<td class="first">接口类:</td>
								<td class="second">
									<select id="apiTypeNameSelect" name="apiTypeName">
									</select>
								</td>
							</tr>
							<tr>
								<td class="first">请求方式:</td>
								<td class="second">
									<select id="apiReqMode" name="apiReqMode">
										<option value="GET">GET</option>
										<option value="POST">POST</option>
									</select>
								</td>
							</tr>
						</table>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn save"
							onclick="submitAddApi()">保存</button>
						<button type="button" class="btn cancel" data-dismiss="modal"
							aria-hidden="true">取消</button>
					</div>
				</div>
				<!--弹窗 -->
			</div>
			<!-- 弹窗 -->
		</div>
		
			<!-- 弹窗 -->
		<div class="modal fade" id="addTypeModal" tabindex="-1" role="dialog"
			aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"
							aria-hidden="true">&times;</button>
						<h4 class="modal-title" id="myModalLabel">新增类型</h4>
					</div>
					<div class="modal-body">
						<table id="addTypeTable">
							<tr>
							<td>类名：</td>
							<td><input id="typeNameNew" name="typeName" placeholder="请输入模块的英文名建类"></td>
							<tr>
							<tr>
							<td>描述：</td>
							<td><input id="typeDesc" name="typeDesc" placeholder="请输入该类的描述"></td>
							<tr>
						</table>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn save"
							onclick="submitAddType()" >保存</button>
						<button type="button" class="btn cancel" data-dismiss="modal"
							aria-hidden="true">取消</button>
					</div>
				</div>
				<!--弹窗 -->
			</div>
			<!-- 弹窗 -->
		</div>
	</div>
	<script src="/static/jquery/jquery-3.2.1.min.js" type="text/javascript"></script>
	<script src="/static/bootstrap/3.3.4/js/bootstrap.min.js" type="text/javascript"></script>
  	<script src="/static/bootstrap-datetimepicker/js/bootstrap-datetimepicker-secondselect.js" type="text/javascript"></script><%--修改过的datetimepicker(支持选择秒数,需配置format带秒数方可显示)--%>
	<script src="/static/bootstrap-datetimepicker/js/locales/bootstrap-datetimepicker.zh-CN.js" type="text/javascript"></script>
	<script src="/static/bootstrap-table/bootstrap-table.min.js" type="text/javascript"></script>
	<script src="/static/bootstrap-table/locale/bootstrap-table-zh-CN.min.js" type="text/javascript"></script>
	<script src="/static/bootstrap-table/extensions/resizable/bootstrap-table-resizable.js" type="text/javascript"></script>
	<script src="/static/jquery-colresizable/colResizable-1.6.js" type="text/javascript"></script>
	<script src="/static/bootstrap-sweetalert/sweetalert.min.js" type="text/javascript"></script>
	<script src="/static/moudle/api/index.js?r=<%=new java.util.Random().nextInt(100) %>" type="text/javascript"></script>
	</body>
</html>