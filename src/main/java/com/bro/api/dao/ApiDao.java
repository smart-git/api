package com.bro.api.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;
import org.springframework.stereotype.Repository;

import com.bro.api.entity.Api;
import com.bro.api.entity.ApiType;

/**
 * 
 * @ClassName: ApiDao
 * @Description: 接口持久层
 * @author Administrator
 * @date 2018年8月27日
 *
 */
@Repository
public interface ApiDao {
	void insert(Api api);

	/**
	 * 
	 * @Title: findList @Description: 分页查找接口 api @param @param rb @param @return
	 *         参数 @return List<Api> 返回类型 @throws
	 */
	List<Api> findList(Api api, RowBounds rb);

	Integer findListCount(Api api);
	//导出
	List<Api> findList(Api api);
	/**
	 * 
		* @Title: findByTypeName
		* @Description: 查找类型相关
		* @param @return 参数
		* @return Integer 返回类型
		* @throws
	 */
	Integer findByTypeName(@Param("apiTypeName")String apiTypeName);
	Integer getMaxType();
	Integer getMaxOrder(@Param("apiType")int apiType);
	
	String findResultByMethod(@Param("methodName")String methodName);

	/**
	 * 
	 * @Title: getAllNoZhuCeApi @Description: 得到所有未注册的api接口
	 * 参数 @return List<String> 返回类型 @throws
	 */
	List<Api> getAllNoZhuCeApi();
	/**
	 * 
	* @Title: insertApiType
	* @Description: 插入类型
	* @param @param at    参数
	* @return void    返回类型
	* @throws
	 */
	void insertApiType(ApiType at);
	List<ApiType> getAllTypes();
}
