package com.bro.api.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bro.api.dao.ApiDao;
import com.bro.api.entity.Api;
import com.bro.api.entity.ApiType;

/**
 * 
 * @ClassName: ApiService
 * @Description: Api接口Service
 * @author Administrator
 * @date 2018年8月27日
 *
 */
@Service
@Transactional
public class ApiService {
	private Logger logger = LoggerFactory.getLogger(getClass());
	@Autowired
	private ApiDao apiDao;

	public void insertApi(Api api) {
		api.setApiUseFlag(1);
		api.setApiDelFlag(1);
		Integer apiType=apiDao.findByTypeName(api.getApiTypeName());
		api.setApiType(apiType);
		Integer orderMax=apiDao.getMaxOrder(apiType);
		api.setApiOrder(orderMax==null?1:(orderMax+1));
		logger.info("插入接口为:" + api.toString());
		apiDao.insert(api);
	}

	public Map<String, Object> findListPage(Api api,RowBounds rb) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.clear();
		List<Api> list = apiDao.findList(api, rb);
		map.put("rows", list);
		Integer total = apiDao.findListCount(api);
		map.put("total", total);
		return map;
	}

	public String getByMethod(String methodName) {
		return apiDao.findResultByMethod(methodName);
	}

	public List<Api> getAllNoZhuCeApi() {
		return apiDao.getAllNoZhuCeApi();
	}
	public List<Api> exportApi(Api api){
		return apiDao.findList(api);
	}
	public void insertApiType(ApiType at) {
		apiDao.insertApiType(at);
	}
	public List<ApiType>  getAllTypes(){
		return apiDao.getAllTypes();
	}
}
