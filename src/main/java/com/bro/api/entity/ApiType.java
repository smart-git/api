package com.bro.api.entity;

import java.io.Serializable;

/**
 * 
 * @ClassName: ApiType
 * @Description: 类型处理
 * @author Administrator
 * @date 2018年9月7日
 *
 */
public class ApiType implements Serializable {
	private static final long serialVersionUID = 1L;
	private int id;
	private String typeName;
	private String typeDesc;
	private int order;
	private String date;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public String getTypeDesc() {
		return typeDesc;
	}

	public void setTypeDesc(String typeDesc) {
		this.typeDesc = typeDesc;
	}

	public int getOrder() {
		return order;
	}

	public void setOrder(int order) {
		this.order = order;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

}
