package com.bro.api.entity;

import java.io.Serializable;

import com.bro.api.utils.excel.annotation.ExcelField;

/**
 * 
 * @ClassName: Api
 * @Description: api实体类
 * @author Administrator
 * @date 2018年8月27日
 *
 */
public class Api implements Serializable {

	private static final long serialVersionUID = 1L;
	private int apiId;// 接口ID
	private String apiMethod;// 接口方法
	private String apiParams;// 接口参数
	private String apiResultContent;// 接口返回内容
	private String apiDescription;// 接口描述
	private int apiUseFlag;// 接口注册标志0未，1是的
	private int apiDelFlag;// 接口删除标志0未，1删除
	private String apiTime;// 接口生成时间
	private int apiType;
	private String apiTypeName;
	private String apiReqMode;
	private int apiOrder;
	private String remark;

	public int getApiId() {
		return apiId;
	}

	public void setApiId(int apiId) {
		this.apiId = apiId;
	}

	@ExcelField(title = "接口方法",align=2,sort=1)
	public String getApiMethod() {
		return apiMethod;
	}

	public void setApiMethod(String apiMethod) {
		this.apiMethod = apiMethod;
	}
	@ExcelField(title = "接口返回json",align=2,sort=2)
	public String getApiResultContent() {
		return apiResultContent;
	}

	public void setApiResultContent(String apiResultContent) {
		this.apiResultContent = apiResultContent;
	}
	@ExcelField(title = "接口描述",align=2,sort=3)
	public String getApiDescription() {
		return apiDescription;
	}

	public void setApiDescription(String apiDescription) {
		this.apiDescription = apiDescription;
	}
	@ExcelField(title = "接口注册标志1表注册",align=2,sort=4)
	public int getApiUseFlag() {
		return apiUseFlag;
	}

	public void setApiUseFlag(int apiUseFlag) {
		this.apiUseFlag = apiUseFlag;
	}
	@ExcelField(title = "接口删除标志1表未删除",align=2,sort=5)
	public int getApiDelFlag() {
		return apiDelFlag;
	}

	public void setApiDelFlag(int apiDelFlag) {
		this.apiDelFlag = apiDelFlag;
	}
	@ExcelField(title = "接口生成时间",align=2,sort=6)
	public String getApiTime() {
		return apiTime;
	}

	public void setApiTime(String apiTime) {
		this.apiTime = apiTime;
	}
	@ExcelField(title = "接口参数列表",align=2,sort=7)
	public String getApiParams() {
		return apiParams;
	}

	public void setApiParams(String apiParams) {
		this.apiParams = apiParams;
	}
	@ExcelField(title = "接口类型序号",align=2,sort=8)
	public int getApiType() {
		return apiType;
	}

	public void setApiType(int apiType) {
		this.apiType = apiType;
	}
	@ExcelField(title = "接口类型名称",align=2,sort=9)
	public String getApiTypeName() {
		return apiTypeName;
	}

	public void setApiTypeName(String apiTypeName) {
		this.apiTypeName = apiTypeName;
	}
	@ExcelField(title = "接口请求方式",align=2,sort=10)
	public String getApiReqMode() {
		return apiReqMode;
	}

	public void setApiReqMode(String apiReqMode) {
		this.apiReqMode = apiReqMode;
	}

	@ExcelField(title = "接口排序",align=2,sort=11)
	public int getApiOrder() {
		return apiOrder;
	}

	public void setApiOrder(int apiOrder) {
		this.apiOrder = apiOrder;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Override
	public String toString() {
		return "Api [apiId=" + apiId + ", apiMethod=" + apiMethod + ", apiParams=" + apiParams + ", apiResultContent="
				+ apiResultContent + ", apiDescription=" + apiDescription + ", apiUseFlag=" + apiUseFlag
				+ ", apiDelFlag=" + apiDelFlag + ", apiTime=" + apiTime + ", apiType=" + apiType + ", apiTypeName="
				+ apiTypeName + ", apiOrder=" + apiOrder + ", remark=" + remark + "]";
	}

}
