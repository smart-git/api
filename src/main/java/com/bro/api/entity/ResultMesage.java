package com.bro.api.entity;

public class ResultMesage {
	private String code = "0";
	private String message = "成功";
	private String result;

	public ResultMesage() {

	}

	public ResultMesage(String result) {
		this.result = result;
	}

	public ResultMesage(String code, String message, String result) {
		this.code = code;
		this.message = message;
		this.result = result;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

}
