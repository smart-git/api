package com.bro.api.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bro.api.entity.ResultMesage;
import com.bro.api.utils.BaseController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;

@Controller
@RequestMapping("api")
@Api(value = "apiactivity", description = "所有的act接口信息操作") 
public class ApiActivityController extends BaseController {

	@RequestMapping("v1.homepage/latestactivity")
	@ResponseBody
	@ApiResponse(code = 200, message = "success", response = ResultMesage.class)
	@ApiOperation(value = "获取所有activity信息", httpMethod = "GET", response = String.class, notes = "获取所有activity信息")
	public ResultMesage latestactivity() {
		ResultMesage rm = new ResultMesage("");
		return rm;
	}
	@RequestMapping(value="v1.homepage/latesta125")
	@ResponseBody
	@ApiResponse(code = 200, message = "success", response = ResultMesage.class)
	@ApiOperation(value = "获取主页信息", httpMethod = "GET", response = String.class, notes = "获取主页信息")
	public ResultMesage v1_homepage_latesta125(String name, String sex) {
		String result = apiService.getByMethod("v1.homepage/latesta125");
		logger.info("返回结果为" + result);
		ResultMesage rm = new ResultMesage(result);
		return rm;
	}
}