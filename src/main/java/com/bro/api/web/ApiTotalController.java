package com.bro.api.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.bro.api.utils.BaseController;

import io.swagger.annotations.Api;

@Controller
@RequestMapping("api")
@Api(value = "ApiTotalController", description = "不知道的接口类") 
public class ApiTotalController extends BaseController {
	
}