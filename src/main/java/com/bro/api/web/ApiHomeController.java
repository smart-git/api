package com.bro.api.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bro.api.entity.ResultMesage;
import com.bro.api.utils.BaseController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;

@Controller
@RequestMapping("api")
@Api(value = "ApiHomeController", description = "主页接口类") 
public class ApiHomeController extends BaseController {

	@RequestMapping("v1.homepage/latestactivityTest")
	@ResponseBody
	@ApiResponse(code = 0, message = "success", response = ResultMesage.class)	@ApiOperation(value = "测试接口", httpMethod = "GET", response = String.class, notes = "测试接口")	public ResultMesage v1_homepage_latestactivityTest(String cs){
		String result = apiService.getByMethod("v1.homepage/latestactivityTest");
		logger.info("返回结果为"+result);
		ResultMesage rm = new ResultMesage(result);
	return rm;
	}
	@RequestMapping("v1.homepage/latestactivityTestTwo")
	@ResponseBody
	@ApiResponse(code = 0, message = "success", response = ResultMesage.class)
	@ApiOperation(value = "测试接口二", httpMethod = "GET", response = String.class, notes = "测试接口二")
	public ResultMesage v1_homepage_latestactivityTestTwo(String name,String age,String cs){
		String result = apiService.getByMethod("v1.homepage/latestactivityTestTwo");
		logger.info("返回结果为"+result);
		ResultMesage rm = new ResultMesage(result);
	return rm;
	}
}