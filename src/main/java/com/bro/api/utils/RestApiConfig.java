package com.bro.api.utils;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
 
@EnableSwagger2 //使swagger2生效  
@ComponentScan(basePackages = {"com.bro.api.web"}) //需要扫描的包路径  
@Configurable //配置注解，自动在本类上下文加载一些环境变量信息  
public class RestApiConfig extends WebMvcConfigurationSupport {
 
	   @Bean
	    public Docket buildDocket(){
	        return new Docket(DocumentationType.SWAGGER_2)
	                .apiInfo(buildApiInf())
	                .select().apis(RequestHandlerSelectors.basePackage("com.bro.api.web"))//controller路径
	                .paths(PathSelectors.any())
	                .build();
	    }
 
	    private ApiInfo buildApiInf(){
	        return new ApiInfoBuilder()
	                .title("SpringMVC中使用Swagger2整合")
	                .termsOfServiceUrl("https://blog.csdn.net/qq_21160839")
	                .description("springmvc swagger2")
	                .contact(new Contact("likun", "https://blog.csdn.net/qq_21160839", "2608976812@qq.com"))
	                .build();
 
	    }
}
