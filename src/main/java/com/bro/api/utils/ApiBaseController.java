package com.bro.api.utils;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.ibatis.session.RowBounds;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bro.api.entity.Api;
import com.bro.api.entity.ApiType;
import com.bro.api.utils.excel.ExportExcel;

@Controller
@RequestMapping("api")
public class ApiBaseController extends BaseController {

	@RequestMapping(value = { "", "index" })
	public String apiMain() {
		return "moudle/api/index";
	}

	@RequestMapping("apiRegister")
	@ResponseBody
	public Map<String, Boolean> insertApi(Api api) {
		Map<String, Boolean> map = new HashMap<String, Boolean>();
		map.clear();
		try {
			apiService.insertApi(api);
			/**
			 * 方法内容
			 */
			String methodContent = MethodUtils.newMethodContent(api.getApiMethod(), api.getApiParams(),api.getApiDescription(),api.getApiReqMode());
			/**
			 * 加新方法
			 */
			String fileName = workPath + "\\api\\src\\main\\java\\com\\bro\\api\\web\\"+api.getApiTypeName()+".java";
			MethodUtils.appendMethodA(fileName, methodContent);
			map.put("result", true);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("插入接口错误！" + e);
			map.put("result", false);
		}
		return map;
	}

	/**
	 * 
	 * @Title: findListPage @Description: 分页查找 api @param @param
	 *         pageSize @param @param pageIndex @param @return 参数 @return
	 *         Map<String,Object> 返回类型 @throws
	 */
	@RequestMapping("findListPage")
	@ResponseBody
	public Map<String, Object> findListPage(Api api, Integer pageSize, Integer pageIndex) {
		RowBounds rb = this.getRowBounds(pageIndex, pageSize);
		return apiService.findListPage(api, rb);
	}
	/**
	 * 
	* @Title: exportApi
	* @Description: 导出接口信息
	* @param @param api
	* @param @param response    参数
	* @return void    返回类型
	* @throws
	 */
	@RequestMapping("exportApi")
	public void exportApi(Api api, HttpServletResponse response) {
		List<Api> list = apiService.exportApi(api);
		String fileName = "API接口信息" + DateUtils.getDate("yyyyMMddHHmmss") + ".xlsx";
		try {
			new ExportExcel("API接口信息", Api.class).setDataList(list).write(response, fileName).dispose();
			logger.info("导出API接口信息成功!");
		} catch (IOException e) {
			logger.error("导出API接口信息出错" + e);
			e.printStackTrace();
		}
	}
	/**
	 * 
	* @Title: 插入新类表中
	* @Description: TODO(这里用一句话描述这个方法的作用)
	* @param @param at
	* @param @return    参数
	* @return Map<String,Boolean>    返回类型
	* @throws
	 */
	@RequestMapping("insertApiType")
	@ResponseBody
	public Map<String, Boolean> insertApiType(ApiType at) {
		Map<String, Boolean> map=new HashMap<String, Boolean>();
		map.clear();
		try {
			String filePath = workPath + "\\api\\src\\main\\java\\com\\bro\\api\\web";
			String fileName=at.getTypeName();
			String fileDec=at.getTypeDesc();
			boolean result=MethodUtils.fileIsExist(filePath, fileName);
			if(result) {
				logger.info("---文件已经存在了！---");
				map.put("result", false);
				return map;
			}else {
				boolean resultc=MethodUtils.creatFile(filePath, fileName);
				if(resultc) {
					logger.info("创建"+fileName+".java文件成功!");
					/**
					 * 写入固定部分语句
					 */
					MethodUtils.writeFixedFontent(filePath, fileName, fileDec);
					apiService.insertApiType(at);
					map.put("result", true);
					return map;
				}else {
					logger.info("创建"+fileName+".java文件失败!");
					map.put("result", false);
					return map;
				}
			}
		}catch (Exception e) {
			logger.error("创建新类信息出错" + e);
			map.put("result", false);
		}
		return map;
	}
	/**
	 * 
	* @Title: getAllType
	* @Description: TODO得到所有的类型
	* @param @return    参数
	* @return List<ApiType>    返回类型
	* @throws
	 */
	@RequestMapping("getAllType")
	@ResponseBody
	public List<ApiType> getAllType(){
		return apiService.getAllTypes();
	}
}
