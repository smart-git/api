package com.bro.api.utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.RandomAccessFile;

/**
 * 
 * @ClassName: MethodUtils
 * @Description: 方法追加类
 * @author Administrator
 * @date 2018年8月27日
 *
 */
public class MethodUtils {
	/**
	 * 
	* @Title: fileIsExist
	* @Description: 判断文件是否存在,并创建
	* @param @param filePath:文件路径
	* @param @param fileName:文件名称
	* @param @return    参数
	* @return boolean    返回类型
	* @throws
	 */
	public static boolean fileIsExist(String filePath,String fileName) {
		File file=new File(filePath+File.separator+fileName+".java");
		if(file.exists()) {
			return true;
		}else {
			return false;
		}
	}
	/**
	 * 
	* @Title: creatFile
	* @Description:创建文件
	* @param @param filePath:文件路径
	* @param @param fileName:文件名称
	* @param @return    参数
	* @return boolean    返回类型
	* @throws
	 */
	public static boolean creatFile(String filePath,String fileName) {
		try {
			File file=new File(filePath+File.separator+fileName+".java");
			file.createNewFile();
			return true;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}
	public static String getFixCon(String fileName,String typeDec) {
		String result = "package com.bro.api.web;\n\n" 
				+ "import org.springframework.stereotype.Controller;\n"
				+ "import org.springframework.web.bind.annotation.RequestMapping;\n"
				+ "import org.springframework.web.bind.annotation.ResponseBody;\n\n"
				+ "import com.bro.api.entity.ResultMesage;\n" 
				+ "import com.bro.api.utils.BaseController;\n\n"
				+ "import io.swagger.annotations.Api;\n"
				+ "import io.swagger.annotations.ApiOperation;\n"
				+ "import io.swagger.annotations.ApiResponse;\n\n"
				+ "@Controller\n"
				+ "@RequestMapping(\"api\")\n"
				+ "@Api(value = \""+fileName+"\", description = \""+typeDec+"\") \n"
				+ "public class "+fileName+" extends BaseController {\n\n}";
		
		return result;
	}
	/**
	 * 
	* @Title: writeFixedFontent
	* @Description: 写入固定内容
	* @param @return    参数
	* @return boolean    返回类型
	* @throws
	 */
	public static boolean writeFixedFontent(String filePath,String fileName,String typeDec) {
		boolean flag = false;
		BufferedWriter out = null;
		try {
			File file = new File(filePath+File.separator+fileName+".java");
			out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), "UTF-8"));
			String str=getFixCon(fileName, typeDec);
			out.write(str);
			out.flush();
			flag=true;
		}catch (Exception e) {
			e.printStackTrace();
		}finally {
			if(out!=null) {
				try {
					out.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return flag;
	}
	public static void appendMethodA(String fileName, String content) {
		try {
			// 打开一个随机访问文件流，按读写方式
			RandomAccessFile randomFile = new RandomAccessFile(fileName, "rw");
			// 文件长度，字节数
			long fileLength = randomFile.length();
			// 将写文件指针移到文件尾。
			randomFile.seek(fileLength - 1);
			// randomFile.writeBytes(content);
			randomFile.write(content.getBytes());
			randomFile.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	/**
	 * 
	* @Title: newMethodContent
	* @Description: 方法内容拼接
	* @param @param methodName:方法名称
	* @param @param params：方法参数
	* @param @param methodDec：方法描述
	* @param @param methodMode：方法请求方式
	* @param @return    参数
	* @return String    返回类型
	* @throws
	 */
	public static String newMethodContent(String methodName, String params,String methodDec,String methodMode) {
		StringBuilder methodContent = new StringBuilder("");
		methodContent.append("\t@RequestMapping(\""+methodName+"\")\n");
		methodContent.append("\t@ResponseBody\n");
		methodContent.append("\t@ApiResponse(code = 0, message = \"success\", response = ResultMesage.class)\n");
		methodContent.append("\t@ApiOperation(value = \""+methodDec+"\", httpMethod = \""+methodMode+"\", response = String.class, notes = \""+methodDec+"\")\n");
		String method=methodName.replaceFirst("\\.", "_")
		.replaceAll("/", "_");
		String newParams="";
		if(params!=null) {
			params=params.trim();
			String[] newkeyPs=params.split(";");
			for (int i=0;i<newkeyPs.length;i++) {
				String[] kp=newkeyPs[i].split(",");
				if(i!=(newkeyPs.length-1)) {
					newParams=newParams+kp[0]+" "+kp[1]+",";
				}else {
					newParams=newParams+kp[0]+" "+kp[1];
				}
			}
		}
		methodContent.append("\tpublic ResultMesage "+method+"("+newParams+"){\n");
		methodContent.append("\t\tString result = apiService.getByMethod(\""+methodName+"\");\n");
		methodContent.append("\t\tlogger.info(\"返回结果为\"+result);\n");
		methodContent.append("\t\tResultMesage rm = new ResultMesage(result);\n");
		methodContent.append("\treturn rm;\n");
		methodContent.append("\t}\n");
		methodContent.append("}");
		return methodContent.toString();
	}
}
