package com.bro.api.utils;

import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.bro.api.service.ApiService;

/**
 * 控制器支持类	
 * @author boco
 * @version 2013-3-23
 */
public abstract class BaseController{

	/**
	 * 日志对象
	 */
	protected Logger logger = LoggerFactory.getLogger(getClass());
	@Autowired
	protected ApiService apiService;
	@Value("${workPath}")
	protected String workPath;
	public RowBounds getRowBounds(Integer page, Integer rows) {
		if (page == null || rows == null) {
			return new RowBounds(0, 15);
		}
		if (page < 1)
			page = 1;
		if (rows < 1)
			rows = 15;
		return new RowBounds((page - 1) * rows,rows);
	}
}
